using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    private const string GameSceneName = "Game";
    private const string MenuSceneName = "Menu";
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject winMenu;

    private bool _isShowing;
    private bool _winShown;
    
    public void StartGame()
    {
        SceneManager.LoadSceneAsync(GameSceneName);
    }
    
    public void QuitGame()
    {
        Time.timeScale = 1;
        Debug.Log("Game has been quit.");
        SceneManager.LoadScene(MenuSceneName);
    }

    public void ClosePauseMenu()
    {
        if (pauseMenu)
        {
            pauseMenu.SetActive(false);
            Time.timeScale = 1;
            _isShowing = false;
        }
    }

    public void ShowWinMenu()
    {
        if (winMenu && !_winShown)
        {
            winMenu.SetActive(true);
            Time.timeScale = 0;
            _isShowing = true;
            _winShown = true;
        }
    }
    
    public void CloseWinMenu()
    {
        if (winMenu)
        {
            winMenu.SetActive(false);
            Time.timeScale = 1;
            _isShowing = false;
        }
    }

    private void Start()
    {
        // CardCountController.OnAllCardsFound += ShowWinMenu;
    }

    private void OnDestroy()
    {
        // CardCountController.OnAllCardsFound -= ShowWinMenu;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!pauseMenu && !winMenu)
            {
                return;
            }

            if (_isShowing)
            {
                ClosePauseMenu();
                CloseWinMenu();
            }
            else
            {
                pauseMenu.SetActive(true);
                Time.timeScale = 0;
                _isShowing = true;
            }
        }
    }
}